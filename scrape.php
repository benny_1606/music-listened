<?php

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/config.php';

use \Curl\Curl;
use \Curl\MultiCurl;

$time = time();
$time_expire = apcu_fetch(__FILE__.'_time_expire');

if(is_int($time_expire) || $time_expire < $time) //Ampache auth stuff
{

	$user = AMP_USER;
	$key = hash('sha256', AMP_KEY);
	$passphrase = hash('sha256', $user. $key);

	$curl = new Curl();
	$curl->get(AMP_BASE_URL.'/server/json.server.php', array(
		'auth'		=> $passphrase,
		'client'	=> 'ihm',
		'version'	=> '400004',
		'action'	=> 'handshake'
		));

	//var_dump($curl->response);

	$date_expire = DateTime::createFromFormat(DateTime::ATOM, $curl->response->{'session_expire'});
	$time_expire = (int) $date_expire->format('U');

	$auth = $curl->response->{'auth'};

	apcu_store(__FILE__.'_time_expire', $time_expire);
	apcu_store(__FILE__.'_auth', $auth);

}
else
{
	$auth = apcu_fetch(__FILE__.'_auth');
}

$curl = new Curl();
$curl->get('https://libre.fm/2.0/', array( // Scrape from Librefm
	'method'	=> 'user.getrecenttracks',
	'user'		=> LIBREFM_USER,
	'page'		=> '1',
	'limit'		=> LIBREFM_LIMIT,
	'format'	=> 'json'
	));
$fm_recenttracks = $curl->response;

$db = new MysqliDb (Array (
                'host' => MYSQL_HOST,
                'username' => MYSQL_USER,
                'password' => MYSQL_PASS,
                'db'=> MYSQL_DB,
                'port' => MYSQL_PORT,
                'prefix' => '',
                'charset' => 'utf8'));

// Get the last record, to prevent duplication.
$db->orderBy("time","Desc");
$last_track_array = $db->getOne('tracks', 'CONCAT(artist,title,album) as ata, UNIX_TIMESTAMP(time) as time');

$fm_recenttracks_array = $fm_recenttracks->{'recenttracks'}->{'track'};

////$fm_recenttracks_array = array_reverse($fm_recenttracks_array);

$data = array();

$multi_curl = new MultiCurl();

$multi_curl->success(function($instance) {
    echo 'call to "' . $instance->url . '" was successful.' . "\n";
    echo 'response:' . "\n";
    var_dump($instance->response);
});
$multi_curl->error(function($instance) {
    echo 'call to "' . $instance->url . '" was unsuccessful.' . "\n";
    echo 'error code: ' . $instance->errorCode . "\n";
    echo 'error message: ' . $instance->errorMessage . "\n";
});
$multi_curl->complete(function($instance) {
    echo 'call completed' . "\n";
});

foreach($fm_recenttracks_array as $track) {
	$date_raw = $track->{'date'}->{'uts'};
	$artist = $track->{'artist'}->{'#text'};
	$name = $track->{'name'};
	$album = $track->{'album'}->{'#text'};
	$ata = $artist.$name.$album;
	// $url = urldecode($track->{'url'});
	//'https://libre.fm/artist/'.urlencode($artist).'/album/'.urlencode($album).'/track/'.urlencode($name)

	if(intval($date_raw) > $last_track_array['time'] && strcmp(strval($last_track_array['ata']),strval($ata)) !== 0 && !isset($track->{'@attr'}->{'nowplaying'})) {
		//var_dump($track);
    
    //$ref = uniqid(NULL, TRUE);
    if(strval($album) > 0) {
			$multi_curl->addGet(AMP_BASE_URL.'/server/json.server.php', array( // scrobble to Ampache
				'auth'		=> $auth,
				'client'	=> 'ihm',
				'version'	=> '400004',
				'action'	=> 'scrobble',
				'song'		=> $name,
				'artist'	=> $artist,
				'album'		=> $album,
				'date'		=> $date_raw
				//'ref'     => $ref
			));
		}

		$data_row = array ( // Also store to MySQL, for analysis later on
			"artist" => $artist,
			"title" => $name,
			"album" => $album,
			"time" => $db->func('FROM_UNIXTIME(?)',array($date_raw))
			);
          
			array_push($data, $data_row);
	}
}

if(isset($data_row)){
  $ins_id = $db->insertMulti ('tracks', $data);

	if (!$ins_id){
		echo 'Error: '. $db->getLastError();
		echo "Last executed query was ". $db->getLastQuery();
		//exit;
	}
	$multi_curl->start();
}