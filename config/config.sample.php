<?php
error_reporting(-1); // Change it if needed

define('LIBREFM_USER', '');
define('AMP_USER', '');
define('AMP_KEY', ''); // Refer to: https://github.com/ampache/ampache/wiki/JSON-methods
define('AMP_BASE_URL', ''); // Only the hostname(+path if installed in sub-dirctory). No trailing slash.

define('MYSQL_HOST', '');
define('MYSQL_PORT', );
define('MYSQL_USER', '');
define('MYSQL_PASS', '');
define('MYSQL_DB', '');

define('LIBREFM_LIMIT', 30); // Limit records requested from Librefm